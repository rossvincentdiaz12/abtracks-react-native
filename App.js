import React, { Component } from 'react'
import {
  StyleSheet, Alert,
  TouchableOpacity,
  Text, Image,
  View,ImageBackground
} from 'react-native'


export default class App extends Component {

  
 render() {
   return (

     <ImageBackground source={require('./assets/background.jpg')} style={styles.container}>
      <View style={styles.header}>
        <Image style={styles.image}
          source={require('./assets/logo.png')}/> 
      </View>  
      
      
    </ImageBackground>

    

    )
  }
}

const styles = StyleSheet.create({
 header:{
   margin: 30,
    marginTop: 50,
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  image: {
    height: 150,
    width: 150,
    alignSelf: 'center',
  }

})
